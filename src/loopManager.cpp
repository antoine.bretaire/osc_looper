#include "osclooper/loopManager.h"
#include <log4cxx/basicconfigurator.h>
#include <iomanip>
#include <chrono>

using namespace std::literals::chrono_literals;

log4cxx::LoggerPtr loopManager::logger = log4cxx::Logger::getLogger("osclooper.loopManager");

loopManager::loopManager()
{
  if(logger->getAllAppenders().size() == 0)
  {
    log4cxx::BasicConfigurator::configure();
  }
}

loopManager::~loopManager()
{
  std::ostringstream output;

  for(auto loop : loops)
  {
    output << "Loop "
           << std::quoted(loop->get_name())
           << " has been removed from the manager";
    delete loop;
    LOG4CXX_DEBUG(logger, output.str());
    output.str("");
  }

  loops.clear();
  output << "Destruct myself";
  LOG4CXX_TRACE(logger, output.str());
}

const std::vector<loop *> *loopManager::get_loops()
{
  return &loops;
}

void loopManager::add(std::chrono::microseconds /*period*/) {}

void loopManager::list()
{
  std::ostringstream output;
  output << std::endl << "Registered loops:";

  for(auto listed_loop : loops)
  {
    output << std::endl << "   " << listed_loop->get_name();
  }

  LOG4CXX_DEBUG(logger, output.str());
}
