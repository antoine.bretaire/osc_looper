#include "osclooper/engineManager.h"
#include <log4cxx/basicconfigurator.h>
#include <iomanip>
#include <chrono>

using namespace std::literals::chrono_literals;

log4cxx::LoggerPtr engineManager::logger = log4cxx::Logger::getLogger("osclooper.engineManager");

engineManager *engineManager::singleton = nullptr;
std::map<std::string, engine *> engineManager::engines;

engineManager::engineManager()
{
  if(logger->getAllAppenders().size() == 0)
  {
    log4cxx::BasicConfigurator::configure();
  }
}

engineManager::~engineManager()
{
  std::ostringstream output;

  for(auto engine : engines)
  {
    delete engine.second;
    output << "Engine "
           << std::quoted(engine.first)
           << " has been removed from the manager";
    LOG4CXX_DEBUG(logger, output.str());
    output.str("");
  }

  engines.clear();
  output << "Destruct myself";
  LOG4CXX_TRACE(logger, output.str());
}

engineManager *engineManager::instance()
{
  if(singleton == nullptr)
  {
    LOG4CXX_DEBUG(logger, "Create new engineManager");
    singleton = new engineManager();
  }

  return singleton;
}

const std::map<std::string, engine *> *engineManager::get_engines()
{
  return &engines;
}

void engineManager::add(std::string name,
                        std::chrono::microseconds period,
                        unsigned short int osc_port)
{
  std::ostringstream output;

  if(engines.size() >= MAX_ENGINES)
  {
    output << "Too much engine created, max " << MAX_ENGINES;
    LOG4CXX_ERROR(logger, output.str());
    throw std::length_error(output.str());
  }
  else if(engines.count(name) != 0)
  {
    output << "Engine name "
           << std::quoted(name)
           << " is already taken. NOT constructed!";
    LOG4CXX_ERROR(logger, output.str());
    throw std::invalid_argument(output.str());
  }
  else
  {
    for(auto engine : engines)
    {
      if(osc_port == engine.second->get_osc_port())
      {
        output << "Engine OSC port number "
               << osc_port
               << " is already taken by the engine "
               << std::quoted(engine.first)
               << ". So, NOT constructed!";
        LOG4CXX_ERROR(logger, output.str());
        throw std::invalid_argument(output.str());
      }
    }

    engines[name] = new engine(name, period, osc_port);
  }
}

void engineManager::list()
{
  std::ostringstream output;
  output << std::endl << "Registered engines:";

  for(auto listed_engine : engines)
  {
    output << std::endl << "   " << listed_engine.second->get_name();
  }

  LOG4CXX_DEBUG(logger, output.str());
}
